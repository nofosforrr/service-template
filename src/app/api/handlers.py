from aiohttp import web
from app.predictor import predict
import logging


async def handler(request: web.Request) -> web.Response:
    request.app['config']
    return web.Response(status=200, text='Some text')


async def upload_handler(request):

    # WARNING: don't do that if you plan to receive large files!
    data = await request.content.read()
    logging.info('data:', data)
    # image = data['image']

    return web.json_response({"prediction": await predict(data)})


async def hello_handler(request: web.Request) -> web.Response:
    return web.Response(text='Hello world')

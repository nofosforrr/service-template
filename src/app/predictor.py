from PIL import Image
import io
import asyncio
import concurrent.futures

def cpu_bound(f):
    im = Image.open(io.BytesIO(f)).convert('1')
    if len(im.getcolors()) >= 2:
        black, white = im.getcolors()

        print(black[0])
        print(white[0])

        blknum = black[0]
        whtnum = white[0]

        if blknum > whtnum:
            output = 'В изображении больше черных пикселов'
        elif blknum == whtnum:
            output = 'В изображении одинаковое количество черных и белых пикселов'
        else:
            output = 'В изображении больше белых пикселов'
    else:
        output = 'Изображение монохромно'
    return output

async def predict(f):
    loop = asyncio.get_event_loop()
    with concurrent.futures.ProcessPoolExecutor() as pool:
        result = await loop.run_in_executor(
            pool, cpu_bound, f)
    return result
